let canvas, ctx, w, h, tree;
let branchChance = [0.08, 0.09, 0.10, 0.11, 0.12, 0.15, 0.3];

function init() {
    canvas = document.querySelector("#canvas");
    ctx = canvas.getContext("2d");
    resizeReset();
    animationLoop();
}

function resizeReset() {
    let firstColumnHeight = document.querySelector('.first-column').offsetHeight;
    w = canvas.width = document.getElementById('canvasContainer').offsetWidth;
    h = canvas.height = firstColumnHeight;
    tree = new Tree();
}


function animationLoop() {
    drawScene();
    requestAnimationFrame(animationLoop);
}

function drawScene() {
    tree.update();
    tree.draw();
}

function getRandomInt(min, max) {
    return Math.round(Math.random() * (max - min)) + min;
}

class Tree {
    constructor() {
        this.x = w * 0.5;
        this.y = h;
        this.branchs = [];
        this.apples = [];
        this.addBranch(this.x, this.y, getRandomInt(6, 8), 180);
    }

    addBranch(x, y, radius, angle) {
        this.branchs.push(new Branch(x, y, radius, angle));
    }

    draw() {
        this.branchs.map((b) => {
            b.draw();
        });

        this.apples.forEach((apple) => {
            apple.draw();
        });
    }

    update() {
        this.branchs.map((b) => {
            b.update();

            // Ajouter une branche lorsque les conditions sont remplies
            if (b.radius > 0 && b.progress > 0.4 && Math.random() < b.branchChance && b.branchCount < 3) {
                let newBranch = {
                    x: b.x,
                    y: b.y,
                    radius: b.radius - 1,
                    angle: b.angle + (Math.random() < 0.5 ? -1 : 1) * getRandomInt(20, 35)
                };
                this.addBranch(newBranch.x, newBranch.y, newBranch.radius, newBranch.angle);

                // Créer une nouvelle pomme à l'emplacement de la nouvelle branche
                this.apples.push(new Apple(newBranch.x, newBranch.y));

                b.branchCount++;
            }
        });

        // Mettre à jour les pommes
        this.apples.forEach((apple) => {
            apple.update();
        });
    }
}

class Branch {
    constructor(x, y, radius, angle) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.angle = angle;
        this.branchReset();
    }

    branchReset() {
        this.sx = this.x;
        this.sy = this.y;
        this.length = this.radius * 20;
        this.progress = 0;
        this.branchChance = branchChance[7 - this.radius];
        this.branchCount = 0;
    }

    draw() {
        if (this.progress > 1 || this.radius <= 0) return;
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2);
        ctx.fillStyle = `rgba(0, 0, 0, 0.5)`;
        ctx.fill();
        ctx.closePath();
    }

    update() {
        let radian = (Math.PI / 180) * this.angle;
        this.x = this.sx + this.length * this.progress * Math.sin(radian);
        this.y = this.sy + this.length * this.progress * Math.cos(radian);

        if (this.radius === 1) {
            this.progress += 0.05;
        } else {
            this.progress += 0.1 / this.radius;
        }

        if (this.progress > 1) {
            this.radius -= 1;
            this.angle += (Math.floor(Math.random() * 3) - 1) * 10;
            this.branchReset();
        }
    }
}

class Apple {
    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.radius = 5; // Rayon de la pomme
        this.color = "#184224"; // Couleur de la pomme
        this.fallSpeed = 0; // Vitesse de chute de la pomme
    }

    draw() {
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.radius, 2, Math.PI * 1);
        ctx.fillStyle = this.color;
        ctx.fill();
        ctx.closePath();
    }

    update() {
        // Mettre à jour la position de la pomme (par exemple, la faire tomber)
        this.y += this.fallSpeed;

        // Dessiner la pomme
        this.draw();
    }
}

window.addEventListener("DOMContentLoaded", init);
window.addEventListener("resize", resizeReset);